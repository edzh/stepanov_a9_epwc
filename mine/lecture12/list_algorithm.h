
#include "binary_counter.h"

#include "merge_linked.h"

template <typename I>
// requires I is Linked Iterator
I reverse_linked(I first, I last, I tail) {
  while (first != last) {
    I next = first;
    ++next;
    set_successor(first, tail);
    tail = first;
    first = next;
  }
  return tail;
}

template <typename I, typename Compare>
// requires I is Linked Iterator
I merge_linked_simple(I first1, I last1, I first2, I last2, Compare cmp) {
  I result = last1;
  while (first1 != last1 && first2 != last2) {
    I tmp = cmp(*first2, *first1) ? first2++ : first1++;
    set_successor(tmp, result);
    result = tmp;
  }
  return reverse_linked(result, last1, first1 == last1 ? first2 : first1);
}

template <typename I, typename Compare>
// I is Linked Iterator
struct mergesort_linked_operation
{
  typedef I argument_type;
  I nil;
  Compare cmp;
  mergesort_linked_operation(I nil, const Compare& cmp) : nil(nil), cmp(cmp) {}
  //I operator()(I x, I y) { return merge_linked_simple(x, nil, y, nil, cmp); }
  I operator()(I x, I y) { return merge_linked_non_empty_test(x, nil, y, nil, cmp).first; }
};

template <typename I, typename Compare>
// I is Linked Iterator
I mergesort_linked(I first, I last, Compare cmp) {
  mergesort_linked_operation<I, Compare> op(last, cmp);
  binary_counter<mergesort_linked_operation<I, Compare> > counter(op, last);
  counter.reserve(16);
  while (first != last) {
    I tmp = first++;
    set_successor(tmp, last);
    counter.add(tmp);
  }
  return counter.reduce();
}

template <typename I0, typename I1>
// requires I0 is Input Iterator
// requires I1 is Singly Linked List Iterator
I1 generate_list(I0 first, I0 last, I1 tail) {  
  if (first == last) return tail;
  push_front(tail, *first++);
  I1 front = tail;
  while (first != last) {
    push_back(tail, *first++);
    ++tail;
  }
  return front;
}

template <typename I, typename Pred>
// requires I is Singly Linked List Iterator
std::pair<I, std::pair<I, I>> partition_list(I first, I last, Pred p) {
  I head1 = last;
  I head2 = last;
  while (first != last) {
    if (p(*first)) {
      I tmp = first++;
      set_successor(tmp, head1);
      head1 = tmp;
    } else {
      I tmp = first++;
      set_successor(tmp, head2);
      head2 = tmp;
    }
  }
  head2 = reverse_linked(head2, last, last); // head2 is now the middle
  head1 = reverse_linked(head1, last, head2); // head1 is now the head

  return std::make_pair(head1, std::make_pair(head2, last));
}

