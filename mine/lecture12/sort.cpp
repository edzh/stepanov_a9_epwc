#include <iostream>
#include <functional>
#include <cstddef>
#include "algorithm.h"
#include "list_pool.h"
#include "list_algorithm.h"

int main() {
  std::vector<int> vec(10);
  random_iota(vec.begin(), vec.end());
  list_pool<int> pool;
  list_pool<int>::iterator nil(pool);
  list_pool<int>::iterator list = generate_list(vec.begin(), vec.end(), nil);
  print_range(list, nil);
  
  auto result = partition_list(list, nil, [](const int& x){
    return x < 5;
  });
  
  print_range(result.first, result.second.first);
}
