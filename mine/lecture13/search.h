
#include <algorithm>
#include <type_traits>
#include <functional>

////////////////////////////////////////////////////////////////////////
// Counted Ranges
////////////////////////////////////////////////////////////////////////

template <typename I, typename N, typename P>
// requires I is an Input Iterator
// requires N is a large enough integral type
// requires P is a Unary Predicate
// requires value type of I is the same as argument type of P
std::pair<I, N> find_if_n(I first, N n, P pred) {
    while (n && !pred(*first)) { ++first; --n; }
    return std::make_pair(first, n);
}

template <typename I, typename N, typename P>
// requires I is an Input Iterator
// requires N is a large enough integral type
// requires P is a Unary Predicate
// requires ValueType(I) is the same as ArgumentType(P)
std::pair<I, N> find_if_not_n(I first, N n, P pred) {
    while (n && pred(*first)) { ++first; --n; }
    return std::make_pair(first, n);
}

// Standard requires true(0) < false(1) order
template <typename I, typename N, typename P>
// requires I is an Input Iterator
// requires N is a large enough integral type
// requires P is a Unary Predicate
// requires ValueType(I) is the same as ArgumentType(P)
bool is_partitioned_n(I first, N n, P pred) {
    std::pair<I, N> middle = find_if_not_n(first, n, pred);
    return find_if(middle.first, middle.second, pred).second == N(0);
}

template <typename I, typename N, typename P>
// requires I is a Forward Iterator
// requires N is a large enough integral type
// requires P is a Unary Predicate
// requires ValueType(I) is the same as ArgumentType(P)
I partition_point_n(I first, N n, P pred) {
    while (n) {
        N half = n >> 1;
        I middle = first;
        advance(middle, half);
        if (pred(*middle)) {
            n -= (half + 1);
            first = ++middle;
        } else {
            n = half;
        }
    }

    return first;
}

template <typename I, typename N, typename R>
// requires I is a Forward Iterator
// requires N is an integral type
// requires R is a StrictWeakOrdering
bool is_sorted_n(I first, N n, R r) {
    if (n == 0) return true;
    I prev = first;
    while (--n && !r(*++first, *prev)) {
      prev = first;
    }
    return n == 0;
}

template <typename I, typename N>
// requires I is a Forward Iterator with totally ordered value type
inline
bool is_sorted_n(I first, N n) {
    typedef typename std::iterator_traits<I>::value_type T;
    return is_sorted_n(first, n, std::less<T>());
}

template <typename R, typename T>
// requires R is StrictWeakOrdering
// requires T is the argument type of R
class lower_bound_predicate {
public:
    lower_bound_predicate(const R& r, const T& a)
        : r(r), a(&a);

    bool operator()(const T& x) { return r(x, *a); }
    
private:
    R r;
    const T* a;
};

template <typename I, typename N, typename R>
// requires I is a Forward Iterator
// requires N is an integral type
// requires R is a WeakStrictOrderding on the value type of I
inline
I lower_bound_n(I first, N n, const typename std::iterator_traits<I>::value_type& a, R r) {
    // precondition: is_sorted(first, n, r)
    typedef typename std::iterator_traits<I>::value_type T;
    return partition_point_n(first, n, lower_bound_predicate<R, T>(r, a));
}

template <typename I, typename N>
// requires I is a Forward Iterator
// requires N is an integral type
inline
I lower_bound_n(I first, N n, const typename std::iterator_traits<I>::value_type& a) {
    // precondition: is_sorted(first, n, r)
    typedef typename std::iterator_traits<I>::value_type T;
    return lower_bound_n(first, n, a, std::less<T>());
}

template <typename R, typename T>
// requires R is a StrictWeakOrdering on T
class upper_bound_predicate {
public:
    upper_bound_predicate(const R& r, const T& a) : r(r), a(&a) {}

    bool operator()(const T& x) { return !r(*a, x); }

private:
    R r;
    const T* a;
};

template <typename I, typename N, typename R>
// requires I is a Forward Iterator
// requires N is an integral type
// requires R is a WeakStrictOrderding on the value type of I
inline
I upper_bound_n(I first, N n, const typename std::iterator_traits<I>::value_type& a, R r) {
    // precondition: is_sorted(first, n, r)
    typedef typename std::iterator_traits<I>::value_type T;
    return partition_point_n(first, n, upper_bound_predicate<R, T>(r, a));
}

template <typename I, typename N>
// requires I is a Forward Iterator
// requires N is an integral type
inline
I upper_bound_n(I first, N n, const typename std::iterator_traits<I>::value_type& a) {
    // precondition: is_sorted(first, n, r)
    typedef typename std::iterator_traits<I>::value_type T;
    return upper_bound_n(first, n, a, std::less<T>());
}

////////////////////////////////////////////////////////////////////////
// Bounded Ranges
////////////////////////////////////////////////////////////////////////

template <typename I, typename Size, typename P>
// requires I is an Input Iterator
// requires P is a Unary Predicate
// requires ValueType(I) is the same as ArgumentType(P)
I find_if(I first, I last, P pred) {
    while (first != last && !pred(*first)) { ++first; }
    return first;
}

template <typename I, typename Size, typename P>
// requires I is an Input Iterator
// requires P is a Unary Predicate
// requires ValueType(I) is the same as ArgumentType(P)
I find_if_not(I first, I last, P pred) {
    while (first != last && pred(*first)) { ++first; }
    return first;
}

// Standard requires true(0) < false(1) order
template <typename I, typename P>
// requires I is an Input Iterator
// requires P is a Unary Predicate
// requires ValueType(I) is the same as ArgumentType(P)
inline
bool is_partitioned(I first, I last, P pred) {
    I middle = find_if_not(first, last);
    return last == find_if(middle, last, pred);
}

template <typename I, typename P>
// requires I is a Forward Iterator
// requires N is a large enough integral type
// requires P is a Unary Predicate
// requires ValueType(I) is the same as ArgumentType(P)
inline
I partition_point(I first, I last, P pred) {
    return partition_point_n(first, distance(first, last), pred);
}

template <typename I, typename R>
// requires I is a Forward Iterator
// requires R is a StrictWeakOrdering
bool is_sorted(I first, I last, R r) {
    if (first == last) return true;
    I prev = first;
    while (++first != last && !r(*first, *prev)) prev = first;
    return first == last;
}

template <typename I>
// requires I is a Forward Iterator with totally ordered value type
inline
bool is_sorted(I first, I last) {
    typedef typename std::iterator_traits<I>::value_type T;
    return is_sorted(first, last, std::less<T>());
}

template <typename I, typename R>
// requires I is a Forward Iterator
// requires R is a WeakStrictOrderding on the value type of I
inline
I lower_bound(I first, I last, const typename std::iterator_traits<I>::value_type& a, R r) {
    // precondition: is_sorted(first, last, r)
    return lower_bound_n(first, distance(first, last), r);
}

template <typename I>
// requires I is a Forward Iterator with a totally ordered Value Type
inline
I lower_bound(I first, I last, const typename std::iterator_traits<I>::value_type& a) {
    // precondition: is_sorted(first, last, r)
    return lower_bound_n(first, distance(first, last));
}

template <typename I, typename R>
// requires I is a Forward Iterator
// requires R is a WeakStrictOrderding on the value type of I
inline
I upper_bound(I first, I last, const typename std::iterator_traits<I>::value_type& a, R r) {
    // precondition: is_sorted(first, n, r)
    typedef typename std::iterator_traits<I>::value_type T;
    return upper_bound_n(first, distance(first, last), a, r);
}

template <typename I>
// requires I is a Forward Iterator
inline
I upper_bound(I first, I last, const typename std::iterator_traits<I>::value_type& a) {
    // precondition: is_sorted(first, n, r)
    typedef typename std::iterator_traits<I>::value_type T;
    return upper_bound(first, distance(first, last), a);
}

////////////////////////////////////////////////////////////////////////
// Advance
////////////////////////////////////////////////////////////////////////

template <typename I, typename N>
// requires I is an Input Iterator
// requires N is a large enough integral type
inline
void advance(I& first, N n) {
    advance(first, n, std::iterator_traits<I>::iterator_category());
}

template <typename I, typename N>
// requires I is an Input Iterator
// requires N is a large enough integral type
void advance(I& first, N n, std::input_iterator_tag) {
    while (n--) ++first;
}

template <typename I, typename N>
// requires I is a Random Access Iterator
// requires N is a large enough integral type
void advance(I& first, N n, std::random_access_iterator_tag) {
    first += n;
}

////////////////////////////////////////////////////////////////////////
// Distance
////////////////////////////////////////////////////////////////////////

template <typename I>
// requires I is an Input Iterator
inline
typename std::iterator_traits<I>::difference_type
distance(I first, I last) {
    return distance(first, last, std::iterator_traits<I>::iterator_category());
}

template <typename I>
// requires I is an Input Iterator
typename std::iterator_traits<I>::difference_type 
distance(I first, I last, std::input_iterator_tag) {
    typename std::iterator_traits<I>::difference_type n(0);
    while (first != last) {
        ++n;
        ++first;
    }
    return n;
}

template <typename I>
// requires I is a Random Access Iterator
typename std::iterator_traits<I>::difference_type 
distance(I first, I last, std::random_access_iterator_tag) {
    return last - first;
}