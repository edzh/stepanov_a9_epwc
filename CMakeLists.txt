project(Epwc)

add_executable(lec9 "lectures/lecture9/min.cpp")
add_executable(lec10 "lectures/lecture10/min.cpp")

add_executable(lec11 "lectures/lecture11/min.cpp")
add_executable(lec11_mine "mine/lecture11/min.cpp")

add_executable(lec12 "lectures/lecture12/sort.cpp")
add_executable(lec12_mine "mine/lecture12/sort.cpp")

add_executable(lec13_mine "mine/lecture13/search.cpp")